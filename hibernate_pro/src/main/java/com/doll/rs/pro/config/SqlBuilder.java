package com.doll.rs.pro.config;


import org.hibernate.boot.spi.MetadataImplementor;
import org.hibernate.dialect.Dialect;
import org.hibernate.mapping.Column;
import org.hibernate.mapping.Constraint;
import org.hibernate.mapping.SimpleValue;
import org.hibernate.mapping.Table;
import org.hibernate.mapping.UniqueKey;

import com.doll.rs.pro.config.HConfig;

public class SqlBuilder {
	
	HConfig config;
	
	public SqlBuilder(HConfig config) {
		super();
		this.config = config;
	}
	@SuppressWarnings("rawtypes")
	public Column buildColumn(Table table,String name,Class typeNameClass,int length,int precision,int scale,boolean nullable,boolean unique){
		return  buildColumn(  table, name, typeNameClass.getName(), length, precision, scale, nullable, unique);
	}
	public Column buildColumn(Table table,String name,String typeNameClass,int length,int precision,int scale,boolean nullable,boolean unique){
		Column mappingColumn = new Column(name);
		mappingColumn.setLength( length );
		if ( precision > 0 ) { 
			mappingColumn.setPrecision( precision );
			mappingColumn.setScale( scale );
		}
		mappingColumn.setNullable( nullable );
		mappingColumn.setSqlType( null );
		mappingColumn.setUnique( unique );
		SimpleValue simpleValue = new SimpleValue(config.getMetadata(), table );
		simpleValue.setTypeName(typeNameClass);
		mappingColumn.setValue(simpleValue);
		return mappingColumn;
	}
	public Table buildTable(String tableName){
		Table table=new Table(tableName);
		return table;
	}
	
	public String[] buildCreateTableSql(Table table){
		Dialect dialect=config.getDialect();
		String[] sqls=dialect.getTableExporter().getSqlCreateStrings( table,config.getMetadata());
		return sqls;
	}
	public String[] buildDropColumn(Table table,Column col){
		Dialect dialect=config.getDialect();
		String colName = col.getQuotedName( dialect );
		return new String[]{"alter table drop column "+colName};
	}
	public String[] buildAddColumnSql(Table table,Column col,boolean primarykey){
		Dialect dialect=config.getDialect();
		MetadataImplementor  metadata=config.getMetadata();
		String colName = col.getQuotedName( dialect );
		String  optype=dialect.getAddColumnString();
		StringBuffer buf=new StringBuffer("alter table "+table.getQuotedName(dialect)+" "+optype+" ");
		buf.append( colName ).append( ' ' );

		if (primarykey) {
			// to support dialects that have their own identity data type
			if ( dialect.getIdentityColumnSupport().hasDataTypeInIdentityColumn() ) {
				buf.append( col.getSqlType( dialect, metadata ) );
			}
			buf.append( ' ' )
					.append( dialect.getIdentityColumnSupport().getIdentityColumnString( col.getSqlTypeCode( metadata ) ) );
		}
		else {
			buf.append( col.getSqlType( dialect, metadata )  );

			String defaultValue = col.getDefaultValue();
			if ( defaultValue != null ) {
				buf.append( " default " ).append( defaultValue );
			}

			if ( col.isNullable() ) {
				buf.append( dialect.getNullColumnString() );
			}
			else {
				buf.append( " not null" );
			}

		}

		if ( col.isUnique() ) {
			String keyName = Constraint.generateName( "UK_", table, col );
			UniqueKey uk = table.getOrCreateUniqueKey( keyName );
			uk.addColumn( col );
			buf.append(
					dialect.getUniqueDelegate()
							.getColumnDefinitionUniquenessFragment( col )
			);
		}

		if ( col.getCheckConstraint() != null && dialect.supportsColumnCheck() ) {
			buf.append( " check (" )
					.append( col.getCheckConstraint() )
					.append( ")" );
		}

		String columnComment = col.getComment();
		if ( columnComment != null ) {
			buf.append( dialect.getColumnComment( columnComment ) );
		}
		return new String[]{buf.toString()};
	}
	public String[] buildModifyColumnSql(Table table,Column col,boolean primarykey){
		Dialect dialect=config.getDialect();
		MetadataImplementor  metadata=config.getMetadata();
		String colName = col.getQuotedName( dialect );
		String  optype="modify column";
		if(dialect.getClass().getName().toLowerCase().contains("sqlserver")){
			optype="alter column";
		}		
		StringBuffer buf=new StringBuffer(" alter table "+table.getQuotedName(dialect)+" "+optype+" ");
		buf.append( colName ).append( ' ' );

		if (primarykey) {
			// to support dialects that have their own identity data type
			if ( dialect.getIdentityColumnSupport().hasDataTypeInIdentityColumn() ) {
				buf.append( col.getSqlType( dialect, metadata ) );
			}
			buf.append( ' ' )
					.append( dialect.getIdentityColumnSupport().getIdentityColumnString( col.getSqlTypeCode( metadata ) ) );
		}
		else  {
			buf.append( col.getSqlType( dialect, metadata )  );

			String defaultValue = col.getDefaultValue();
			if ( defaultValue != null ) {
				buf.append( " default " ).append( defaultValue );
			}

			if ( col.isNullable() ) {
				buf.append( dialect.getNullColumnString() );
			}
			else {
				buf.append( " not null" );
			}

		}

		if ( col.isUnique() ) {
			String keyName = Constraint.generateName( "UK_", table, col );
			UniqueKey uk = table.getOrCreateUniqueKey( keyName );
			uk.addColumn( col );
			buf.append(
					dialect.getUniqueDelegate()
							.getColumnDefinitionUniquenessFragment( col )
			);
		}

		if ( col.getCheckConstraint() != null && dialect.supportsColumnCheck() ) {
			buf.append( " check (" )
					.append( col.getCheckConstraint() )
					.append( ")" );
		}

		String columnComment = col.getComment();
		if ( columnComment != null ) {
			buf.append( dialect.getColumnComment( columnComment ) );
		}
		return new String[]{buf.toString()};
	}
	public String[] buldDropTabeSql(Table table){
		Dialect dialect=config.getDialect();
		String[] sqls=dialect.getTableExporter().getSqlDropStrings(table, config.getMetadata());
		return sqls;
	}
}

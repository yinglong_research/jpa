package com.doll.rs.pro.config;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.spi.MetadataImplementor;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

public class HConfig {
	
	private String id;
	
	private MetadataImplementor metadata;
	
	private Dialect dialect;
	
	private LocalSessionFactoryBean bean;
	
	private SessionFactoryImplementor sessionFactory;
	
	public static HConfig getInstance(String id,DataSource dataSource,Database database,String dialectName){
		if(id==null){
			throw new RuntimeException("数据源定义id不能为空");
		}
		if(!configMap.containsKey(id)){
			synchronized(id.intern()){
				Properties properties=new Properties();
				if(dialectName==null){
					HibernateJpaVendorAdapter vendor=new HibernateJpaVendorAdapter();
					vendor.setDatabase(database);
					properties.putAll(vendor.getJpaPropertyMap());
				}else{
					Properties hibernateProperties =new Properties();
					hibernateProperties.setProperty("hibernate.dialect", dialectName);
				}
				HConfig c= new HConfig(dataSource,properties);
				configMap.put(id, c);
			}
		}
		return configMap.get(id);
	}
	private HConfig(DataSource dataSource,Properties properties){
		bean=new LocalSessionFactoryBean();
		bean.setDataSource(dataSource);
		bean.setMappingLocations(getHbmResource());
		bean.setHibernateProperties(properties);
		try {
			bean.afterPropertiesSet();
			StandardServiceRegistry re=bean.getConfiguration().getStandardServiceRegistryBuilder().build();
			MetadataSources ms=bean.getMetadataSources();
			metadata=(MetadataImplementor)ms.buildMetadata(re);
			sessionFactory=(SessionFactoryImplementor)bean.getObject();
			dialect=sessionFactory.getDialect();
			dialect.getCreateTableString();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private final static Map<String,HConfig> configMap=new ConcurrentHashMap<>();
	private Resource getHbmResource(){
		String str="<!DOCTYPE hibernate-configuration PUBLIC \"-//Hibernate/Hibernate Configuration DTD 3.0//EN\" \"http://www.hibernate.org/dtd/hibernate-configuration-3.0.dtd\">"
		+"<hibernate-configuration>"
			+"<session-factory name=\"foo\">"
			+"<property name=\"show_sql\">true</property>"
			+"</session-factory>"
		+"</hibernate-configuration>";
		return new ByteArrayResource(str.getBytes());
	}
	public SessionFactoryImplementor getSessionFactory() {
		return sessionFactory;
	}

	public MetadataImplementor getMetadata() {
		return metadata;
	}

	public Dialect getDialect() {
		return dialect;
	}
	public String getId() {
		return id;
	}

	
	
}

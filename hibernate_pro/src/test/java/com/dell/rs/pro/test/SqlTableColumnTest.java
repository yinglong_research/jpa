package com.dell.rs.pro.test;

import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.UUID;

import javax.sql.DataSource;
import org.hibernate.boot.spi.MetadataImplementor;
import org.hibernate.mapping.Column;
import org.hibernate.mapping.PrimaryKey;
import org.hibernate.mapping.Table;
import org.hibernate.sql.Insert;
import org.hibernate.sql.Select;
import org.hibernate.sql.Update;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.doll.rs.pro.HibernateApplication;
import com.doll.rs.pro.config.SqlBuilder;
import com.doll.rs.pro.config.HConfig;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=HibernateApplication.class)
public class SqlTableColumnTest {
	
	@Autowired
	JpaProperties jpaProperties;
	
	@Autowired
	DataSource dataSource;
	
	Table table;
	@Autowired
	JdbcTemplate jdbcTemplate;

	HConfig config;
	MetadataImplementor metadata;
	SqlBuilder  buildService;
	@Test
	public void test(){
		Insert insert = new Insert(config.getDialect())
				.setTableName(table.getName());
		Object[] o=new Object[table.getColumnSpan()];
		{
			Iterator<Column> iterator=table.getColumnIterator();
			int i=0;
			while(iterator.hasNext()){
				Column c=iterator.next();
				if("id".equals(c.getName())){
					insert.addColumn(c.getName(), "?");
					o[i++]=UUID.randomUUID().toString();
				}
				if("desc".equals(c.getName())){
					insert.addColumn(c.getName(), "?");
					o[i++]="desc";
				}
				if("createTime".equals(c.getName())){
					insert.addColumn(c.getName(), "?");
					o[i++]=new Date();
				}
				if("age".equals(c.getName())){
					insert.addColumn(c.getName(), "?");
					o[i++]=5;
				}
				if("money".equals(c.getName())){
					insert.addColumn(c.getName(), "?");
					o[i++]=new BigDecimal("12.1");
				}
				if("qq".equals(c.getName())){
					insert.addColumn(c.getName(), "?");
					o[i++]=214425622L;
				}
			}
		}
		String sql=insert.toStatementString();
		System.out.println(sql);
		jdbcTemplate.update(sql, o);
		Update update = new Update( config.getDialect() )
				.setTableName( table.getName() );
		update.addColumn("age","?");
		update.addColumn("createTime","?");
		update.addPrimaryKeyColumn("id", "?");
		System.out.println(update.toStatementString());
		Select select=new Select(config.getDialect());
		select.setSelectClause(" money ").setFromClause(table.getName()).setWhereClause("qq=?");
		System.out.println(select.toStatementString());
		testDropColumn();
	}
	/**
	 * 
	 */
	public void testDropColumn(){
		Column c=buildService.buildColumn(table,"qq1","java.lang.Long", 10,0,0, true, false);
		c.setComment("描述");
		{
			String sql[]=buildService.buildAddColumnSql(table, c, false);
			for(String s:sql){
				System.out.println(s);
			}
		}
		{
			String sql[]=buildService.buildDropColumn(table, c);
			for(String s:sql){
				System.out.println(s);
			}
		}
	}
	@Before
	public void init(){
		config=	HConfig.getInstance("test", dataSource,jpaProperties.getDatabase(), null);
		buildService=new SqlBuilder(config);
		
		table=buildService.buildTable("mytable");
		
		PrimaryKey primaryKey=new PrimaryKey(table);
		table.setPrimaryKey(primaryKey);
		{
			Column c=buildService.buildColumn(table,"id",java.lang.String.class, 36, 0, 0, false, false);
			primaryKey.addColumn(c);
			c.setComment("主键");
			table.addColumn(c);
		}
		{
			Column c=buildService.buildColumn(table,"desc",java.sql.Clob.class, 32, 0, 0, true, false);
			c.setComment("描述");
			table.addColumn(c);
		}
		{
			Column c=buildService.buildColumn(table,"createTime",java.util.Date.class, 32, 0, 0, true, false);
			c.setComment("描述");
			table.addColumn(c);
		}
		{
			Column c=buildService.buildColumn(table,"age","java.lang.Integer", 32, 0, 0, true, false);
			c.setComment("描述");
			table.addColumn(c);
		}
		{
			Column c=buildService.buildColumn(table,"money","java.math.BigDecimal", 10, 7, 2, true, false);
			c.setComment("描述");
			table.addColumn(c);
		}
		{
			Column c=buildService.buildColumn(table,"qq","java.lang.Long", 10,0,0, true, false);
			c.setComment("描述");
			table.addColumn(c);
		}
		this.drop();
		{
			String[] sql=buildService.buildCreateTableSql(table);
			for(String s:sql){
				System.out.println(s);
				jdbcTemplate.execute(s);
			}
		}
	}
	@After
	public void drop(){
		{
			String[] sql=buildService.buldDropTabeSql(table);
			for(String s:sql){
				System.out.println(s);
				jdbcTemplate.execute(s);
			}
		}
	}
	
	
}

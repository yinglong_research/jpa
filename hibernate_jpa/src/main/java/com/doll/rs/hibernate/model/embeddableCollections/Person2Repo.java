package com.doll.rs.hibernate.model.embeddableCollections;

import org.springframework.data.jpa.repository.JpaRepository;

public interface Person2Repo extends JpaRepository<Person2,Long> {
	
}

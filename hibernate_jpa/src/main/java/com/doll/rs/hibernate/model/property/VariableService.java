package com.doll.rs.hibernate.model.property;

import java.math.BigDecimal;

/**
 * 设置或取变量
 * @author windmap
 *
 */
public interface VariableService {
	public void addVariable(String category,String name,Integer value);
	public void addVariable(String category,String name,BigDecimal value);
	public void addVariable(String category,String name,byte[] value);
	public void addVariable(String category,String name,String value);
	public void addVariableLongString(String category,String name,String value);
	public <T> T getVariable(String category,String name);
}

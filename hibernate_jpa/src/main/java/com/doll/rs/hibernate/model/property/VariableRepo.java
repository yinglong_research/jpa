package com.doll.rs.hibernate.model.property;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface VariableRepo extends JpaRepository<VariableModel,String>{
	
	@Query("from VariableModel where category=:category and name=:name ")
	public VariableModel findByCategoryAndName(@Param("category") String category,@Param("name") String name);
	
	
}

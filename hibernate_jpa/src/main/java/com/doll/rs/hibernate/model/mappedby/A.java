package com.doll.rs.hibernate.model.mappedby;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class A {
	@Id
    @GeneratedValue
    private Long id;
	
	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	private List<B> b=new ArrayList<B>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<B> getB() {
		return b;
	}

	public void setB(List<B> b) {
		this.b = b;
	}
	public void addB(B b) {
		this.b.add(b);
	}
	
}

package com.doll.rs.hibernate.model.mappedby;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table
public class A2 {
	@Id
    @GeneratedValue
    private Long id;
	
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	private List<B2> b2=new ArrayList<B2>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<B2> getB2() {
		return b2;
	}

	public void setB2(List<B2> b2) {
		this.b2 = b2;
	}
	public void addB2(B2 b2){
		this.b2.add(b2);
	}

	
}

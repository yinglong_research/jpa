package com.doll.rs.hibernate.model.mappedby;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="b2")
public class B2 {
	@Id
    @GeneratedValue
    private Long id;
	
	@ManyToMany(mappedBy="b2")
	private List<A2> a2=new ArrayList<A2>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<A2> getA2() {
		return a2;
	}

	public void setA2(List<A2> a2) {
		this.a2 = a2;
	}

	
	
}

package com.doll.rs.hibernate.model.mappedby;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BRepo extends JpaRepository<B,Long>{

}

package com.doll.rs.hibernate.build;

import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.hibernate.boot.spi.MetadataImplementor;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.jpa.HibernateEntityManagerFactory;
import org.hibernate.mapping.Column;
import org.hibernate.mapping.SimpleValue;
import org.hibernate.mapping.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.vendor.SpringHibernateJpaPersistenceProvider;
import org.springframework.stereotype.Service;

@Service
public class BuildService {
	@Autowired
	EntityManagerFactory emf;
	
	@SuppressWarnings("rawtypes")
	public Column buildColumn(MetadataImplementor metadata,Table table,String name,Class typeNameClass,int length,int precision,int scale,boolean nullable,boolean unique){
		return  buildColumn( metadata, table, name, typeNameClass.getName(), length, precision, scale, nullable, unique);
	}
	public Column buildColumn(MetadataImplementor metadata,Table table,String name,String typeNameClass,int length,int precision,int scale,boolean nullable,boolean unique){
		Column mappingColumn = new Column(name);
		mappingColumn.setLength( length );
		if ( precision > 0 ) { 
			mappingColumn.setPrecision( precision );
			mappingColumn.setScale( scale );
		}
		mappingColumn.setNullable( nullable );
		mappingColumn.setSqlType( null );
		mappingColumn.setUnique( unique );
		SimpleValue simpleValue = new SimpleValue(metadata, table );
		simpleValue.setTypeName(typeNameClass);
		mappingColumn.setValue(simpleValue);
		return mappingColumn;
	}
	public Table buildTable(String tableName){
		Table table=new Table(tableName);
		return table;
	}
	
	public String[] buildCreateTableSql(MetadataImplementor metadata,Table table){
		HibernateEntityManagerFactory hemf=(HibernateEntityManagerFactory)this.emf;
		SessionFactoryImplementor sessionFactory=hemf.getSessionFactory();
		Dialect dialect=sessionFactory.getDialect();
		String[] sqls=dialect.getTableExporter().getSqlCreateStrings( table,metadata);
		return sqls;
	}
	public String[] buldDropTabeSql(MetadataImplementor metadata,Table table){
		HibernateEntityManagerFactory hemf=(HibernateEntityManagerFactory)this.emf;
		SessionFactoryImplementor sessionFactory=hemf.getSessionFactory();
		Dialect dialect=sessionFactory.getDialect();
		String[] sqls=dialect.getTableExporter().getSqlDropStrings(table, metadata);
		return sqls;
	}
}

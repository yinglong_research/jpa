package com.doll.rs.hibernate.model.property;

import org.springframework.data.jpa.repository.JpaRepository;

public interface VariableBytesRepo extends JpaRepository<VariableBytesModel,String> {
	
}

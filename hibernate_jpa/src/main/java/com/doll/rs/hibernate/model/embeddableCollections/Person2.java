package com.doll.rs.hibernate.model.embeddableCollections;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OrderColumn;

@Entity
public  class Person2 {

    @Id
    @GeneratedValue
    private Long id;

    @ElementCollection
    @OrderColumn(name = "order_id")
    @CollectionTable(name="phone222")
    private List<Phone> phones = new ArrayList<>();

    public List<Phone> getPhones() {
        return phones;
    }
    public void add(Phone phone){
    	phones.add(phone);
    }
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setPhones(List<Phone> phones) {
		this.phones = phones;
	}
    
}
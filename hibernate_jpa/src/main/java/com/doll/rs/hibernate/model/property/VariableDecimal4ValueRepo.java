package com.doll.rs.hibernate.model.property;

import org.springframework.data.jpa.repository.JpaRepository;

public interface VariableDecimal4ValueRepo extends JpaRepository<VariableDecimal4Model,String> {
	
}

package com.doll.rs.hibernate.model.onetomany;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Many {
	@Id
	@GeneratedValue
    private Long id;

	@ManyToOne
	private One one;
	
	public Many(Long id) {
		super();
		this.id = id;
	}

	public Many() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public One getOne() {
		return one;
	}

	public void setOne(One one) {
		this.one = one;
	}
	
	
}

package com.doll.rs.hibernate.model.onetomany;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;

@Entity
public class One {
	@Id
	@GeneratedValue
    private UUID id;
	@Column(length=32)
	private String name;
	@Column(length=32)
	private String nameAlign;
	
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
//	@OrderColumn(name = "order_id")
	private List<Many> many=new ArrayList<Many>();

	


	public List<Many> getMany() {
		return many;
	}

	public void setMany(List<Many> many) {
		this.many = many;
	}
	
	public void add(Many many){
		this.many.add(many);
	}
	public void remove(Many mm){
		this.many.removeIf(m->mm.getId()==m.getId());
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameAlign() {
		return nameAlign;
	}

	public void setNameAlign(String nameAlign) {
		this.nameAlign = nameAlign;
	}
	
	
}

@AnyMetaDef( name= "PropertyMetaDef", metaType = "string", idType = "string",
    metaValues = {
            @MetaValue(value = "String", targetEntity = VariableStringValueModel.class),
            @MetaValue(value = "LongString", targetEntity = VariableLongStringValueModel.class),
            @MetaValue(value = "bytes", targetEntity = VariableBytesModel.class),
            @MetaValue(value = "decimal", targetEntity = VariableDecimal4Model.class),
            @MetaValue(value = "Integer", targetEntity = VariableIntegerValueModel.class)
        }
    )

package com.doll.rs.hibernate.model.property;

import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.MetaValue;
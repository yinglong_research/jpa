package com.doll.rs.hibernate.model.embed;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Field {
	@Id
	@GeneratedValue
	Integer id;

	@Embedded
	Common common;
	
	String name123;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Common getCommon() {
		return common;
	}

	public void setCommon(Common common) {
		this.common = common;
	}

	public String getName123() {
		return name123;
	}

	public void setName123(String name123) {
		this.name123 = name123;
	}
	
	
	
}

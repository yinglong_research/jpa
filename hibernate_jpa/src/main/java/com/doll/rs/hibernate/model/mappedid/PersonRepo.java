package com.doll.rs.hibernate.model.mappedid;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepo extends JpaRepository<Person,Long>{
	
}

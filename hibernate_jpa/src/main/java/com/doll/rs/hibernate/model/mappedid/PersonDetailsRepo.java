package com.doll.rs.hibernate.model.mappedid;

import org.springframework.data.jpa.repository.JpaRepository;



public interface PersonDetailsRepo extends JpaRepository<PersonDetails,Long> {
	
}

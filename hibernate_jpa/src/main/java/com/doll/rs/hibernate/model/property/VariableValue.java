package com.doll.rs.hibernate.model.property;

public interface VariableValue<T> {
	
	public  T getValue();

	public void setValue(T value) ;
	
	
}

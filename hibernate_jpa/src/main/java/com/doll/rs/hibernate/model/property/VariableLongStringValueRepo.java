package com.doll.rs.hibernate.model.property;
 
import org.springframework.data.jpa.repository.JpaRepository;

public interface VariableLongStringValueRepo extends JpaRepository<VariableLongStringValueModel,String>{


}

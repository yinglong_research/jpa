package com.doll.rs.hibernate.model.page;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TestModelRepo extends JpaRepository<TestModel, String> {
	
	
	
}

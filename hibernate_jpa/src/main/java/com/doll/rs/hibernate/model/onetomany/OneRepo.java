package com.doll.rs.hibernate.model.onetomany;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OneRepo extends JpaRepository<One,Long>{
	
}

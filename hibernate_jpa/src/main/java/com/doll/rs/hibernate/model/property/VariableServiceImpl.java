package com.doll.rs.hibernate.model.property;

import java.math.BigDecimal;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class VariableServiceImpl implements VariableService{
	
	@Autowired
	private VariableRepo variableRepo;
	@Autowired
	VariableIntegerValueRepo variableIntegerValueRepo;
	@Autowired
	VariableDecimal4ValueRepo variableDecimal4ValueRepo;
	@Autowired
	VariableLongStringValueRepo variableLongStringValueRepo;
	@Autowired
	VariableStringValueRepo variableStringValueRepo;
	@Autowired
	VariableBytesRepo variableBytesRepo;
	@Override
	public void addVariable(String category, String name, Integer value) {
		VariableModel cariableModel=variableRepo.findByCategoryAndName( category,  name);
		if(cariableModel==null){
			cariableModel=new VariableModel();
			cariableModel.setCategory(category);
			cariableModel.setName(name);
			VariableIntegerValueModel iv=new VariableIntegerValueModel();
			iv.setValue(value);
			variableIntegerValueRepo.save(iv);
			cariableModel.setVariableValue(iv);
			variableRepo.save(cariableModel);
		}else{
			VariableIntegerValueModel iv=(VariableIntegerValueModel)cariableModel.getVariableValue();
			iv.setValue(value);
			variableIntegerValueRepo.save(iv);
		}
	}

	@Override
	public void addVariable(String category, String name, BigDecimal value) {
		VariableModel cariableModel=variableRepo.findByCategoryAndName( category,  name);
		if(cariableModel==null){
			cariableModel=new VariableModel();
			cariableModel.setCategory(category);
			cariableModel.setName(name);
			VariableDecimal4Model iv=new VariableDecimal4Model();
			iv.setValue(value);
			variableDecimal4ValueRepo.save(iv);
			cariableModel.setVariableValue(iv);
			variableRepo.save(cariableModel);
		}else{
			VariableDecimal4Model iv=(VariableDecimal4Model)cariableModel.getVariableValue();
			iv.setValue(value);
			variableDecimal4ValueRepo.save(iv);
		}
	}

	@Override
	public void addVariable(String category, String name, byte[] value) {
		VariableModel cariableModel=variableRepo.findByCategoryAndName( category,  name);
		if(cariableModel==null){
			cariableModel=new VariableModel();
			cariableModel.setCategory(category);
			cariableModel.setName(name);
			VariableBytesModel iv=new VariableBytesModel();
			iv.setValue(value);
			variableBytesRepo.save(iv);
			cariableModel.setVariableValue(iv);
			variableRepo.save(cariableModel);
		}else{
			VariableBytesModel iv=(VariableBytesModel)cariableModel.getVariableValue();
			iv.setValue(value);
			variableBytesRepo.save(iv);
		}
	}

	@Override
	public void addVariable(String category, String name, String value) {
		VariableModel cariableModel=variableRepo.findByCategoryAndName( category,  name);
		if(cariableModel==null){
			cariableModel=new VariableModel();
			cariableModel.setCategory(category);
			cariableModel.setName(name);
			if(value.length()>330){
				VariableLongStringValueModel iv=new VariableLongStringValueModel();
				iv.setValue(value);
				variableLongStringValueRepo.save(iv);
				cariableModel.setVariableValue(iv);
				variableRepo.save(cariableModel);
			}else{
				VariableStringValueModel iv=new VariableStringValueModel();
				iv.setValue(value);
				variableStringValueRepo.save(iv);
				cariableModel.setVariableValue(iv);
				variableRepo.save(cariableModel);
			}
		}else{
			if(cariableModel.getVariableValue() instanceof VariableStringValueModel){
				VariableStringValueModel iv=(VariableStringValueModel)cariableModel.getVariableValue();
				iv.setValue(value);
				variableStringValueRepo.save((VariableStringValueModel)iv);
			}else if(cariableModel.getVariableValue() instanceof VariableLongStringValueModel){
				VariableLongStringValueModel iv=(VariableLongStringValueModel)cariableModel.getVariableValue();
				iv.setValue(value);
				variableLongStringValueRepo.save(iv);
			}
		}
	}

	
	@Override
	public void addVariableLongString(String category, String name, String value) {
		VariableModel cariableModel=variableRepo.findByCategoryAndName( category,  name);
		if(cariableModel==null){
			cariableModel=new VariableModel();
			cariableModel.setCategory(category);
			cariableModel.setName(name);
			VariableLongStringValueModel iv=new VariableLongStringValueModel();
			iv.setValue(value);
			variableLongStringValueRepo.save(iv);
			cariableModel.setVariableValue(iv);
			variableRepo.save(cariableModel);
		}else{
			VariableLongStringValueModel iv=(VariableLongStringValueModel)cariableModel.getVariableValue();
			iv.setValue(value);
			variableLongStringValueRepo.save(iv);
		}
		
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional
	public <T> T getVariable(String category, String name) {
		VariableModel cariableModel=variableRepo.findByCategoryAndName( category,  name);
		if(cariableModel==null){
			return null;
		}
		return (T) cariableModel.getVariableValue().getValue();
	}
	
	

}

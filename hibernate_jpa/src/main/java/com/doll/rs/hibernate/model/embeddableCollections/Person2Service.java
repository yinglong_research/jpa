package com.doll.rs.hibernate.model.embeddableCollections;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Person2Service {
	@Autowired
	Person2Repo person2Repo;
	
	@Transactional
	public Person2 insert(){
		Person2 p=new Person2();
		person2Repo.save(p);
		p.add(new Phone());
		p.add(new Phone());
		p=person2Repo.save(p);
		Person2 pp=person2Repo.findOne(p.getId());
		System.out.println(p.getId()+":"+pp.getPhones().size());
		return p;
	}
}

package com.doll.rs.hibernate.model.property;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.CascadeType;
import javax.persistence.Column;

import org.hibernate.annotations.Any;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table( name = "variable" )
public class VariableModel {
	
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDHexGenerator")
	private String id;
	
	@Column(length=128)
	private String category;
	
	@Column(length=128)
	private String name;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@SuppressWarnings("rawtypes")
	@Any(
        metaDef = "PropertyMetaDef",
        metaColumn = @Column( name = "property_type" )
    )
    @JoinColumn( name = "property_id" )
	private VariableValue variableValue;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@SuppressWarnings("rawtypes")
	public VariableValue getVariableValue() {
		return variableValue;
	}
	@SuppressWarnings("rawtypes")
	public void setVariableValue(VariableValue variableValue) {
		this.variableValue = variableValue;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	
	
}

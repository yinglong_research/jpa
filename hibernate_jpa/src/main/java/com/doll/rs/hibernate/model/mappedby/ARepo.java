package com.doll.rs.hibernate.model.mappedby;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ARepo extends JpaRepository<A,Long>{

}

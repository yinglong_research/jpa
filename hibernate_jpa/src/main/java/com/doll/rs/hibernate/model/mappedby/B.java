package com.doll.rs.hibernate.model.mappedby;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="b")
public class B {
	@Id
    @GeneratedValue
    private Long id;
	
	
	
	public B() {
		super();
		// TODO Auto-generated constructor stub
	}

	public B(Long id) {
		super();
		this.id = id;
	}

	@ManyToOne
	private A a;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public A getA() {
		return a;
	}

	public void setA(A a) {
		this.a = a;
	}
	
	
	
}

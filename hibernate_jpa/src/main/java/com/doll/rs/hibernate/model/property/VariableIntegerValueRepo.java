package com.doll.rs.hibernate.model.property;

import org.springframework.data.jpa.repository.JpaRepository;

public interface VariableIntegerValueRepo extends JpaRepository<VariableIntegerValueModel,String> {
	
}

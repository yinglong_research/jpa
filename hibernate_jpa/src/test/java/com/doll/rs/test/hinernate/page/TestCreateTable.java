package com.doll.rs.test.hinernate.page;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.UUID;

import javax.persistence.EntityManagerFactory;

import org.hibernate.boot.spi.MetadataImplementor;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.jpa.HibernateEntityManagerFactory;
import org.hibernate.mapping.Column;
import org.hibernate.mapping.PrimaryKey;
import org.hibernate.mapping.Table;
import org.hibernate.sql.Insert;
import org.hibernate.sql.Select;
import org.hibernate.sql.Update;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.vendor.SpringHibernateJpaPersistenceProvider;
import org.springframework.test.context.junit4.SpringRunner;

import com.doll.rs.hibernate.HibernateJpaApplication;
import com.doll.rs.hibernate.build.BuildService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = HibernateJpaApplication.class)
public class TestCreateTable {
	@Autowired
	EntityManagerFactory emf;
	@Autowired
	BuildService buildService;
	MetadataImplementor metadata;
	Table table;
	Dialect dialect;
	@Autowired
	JdbcTemplate jdbcTemplate;
	@SuppressWarnings("unchecked")
	@Test
	public void test(){
		Insert insert = new Insert(dialect)
				.setTableName(table.getName());
		Object[] o=new Object[table.getColumnSpan()];
		{
			Iterator<Column> iterator=table.getColumnIterator();
			int i=0;
			while(iterator.hasNext()){
				Column c=iterator.next();
				if("id".equals(c.getName())){
					insert.addColumn(c.getName(), "?");
					o[i++]=UUID.randomUUID().toString();
				}
				if("desc".equals(c.getName())){
					insert.addColumn(c.getName(), "?");
					o[i++]="desc";
				}
				if("createTime".equals(c.getName())){
					insert.addColumn(c.getName(), "?");
					o[i++]=new Date();
				}
				if("age".equals(c.getName())){
					insert.addColumn(c.getName(), "?");
					o[i++]=5;
				}
				if("money".equals(c.getName())){
					insert.addColumn(c.getName(), "?");
					o[i++]=new BigDecimal("12.1");
				}
				if("qq".equals(c.getName())){
					insert.addColumn(c.getName(), "?");
					o[i++]=214425622L;
				}
			}
		}
		String sql=insert.toStatementString();
		System.out.println(sql);
		jdbcTemplate.update(sql, o);
		Update update = new Update( dialect )
				.setTableName( table.getName() );
		update.addColumn("age","?");
		update.addColumn("createTime","?");
		update.addPrimaryKeyColumn("id", "?");
		System.out.println(update.toStatementString());
		Select select=new Select(this.dialect);
		select.setSelectClause(" money ").setFromClause(table.getName()).setWhereClause("qq=?");
		System.out.println(select.toStatementString());
	}
	
	@Before
	public void init(){
		metadata=SpringHibernateJpaPersistenceProvider
				.getEntityManagerFactoryBuilderImpl().getMetadata();
		HibernateEntityManagerFactory hemf=(HibernateEntityManagerFactory)this.emf;
		SessionFactoryImplementor sessionFactory=hemf.getSessionFactory();
		dialect=sessionFactory.getDialect();
		table=buildService.buildTable("mytable");
		
		PrimaryKey primaryKey=new PrimaryKey(table);
		table.setPrimaryKey(primaryKey);
		{
			Column c=buildService.buildColumn(metadata,table,"id",java.lang.String.class, 36, 0, 0, false, false);
			primaryKey.addColumn(c);
			c.setComment("主键");
			table.addColumn(c);
		}
		{
			Column c=buildService.buildColumn(metadata,table,"desc",java.sql.Clob.class, 32, 0, 0, true, false);
			c.setComment("描述");
			table.addColumn(c);
		}
		{
			Column c=buildService.buildColumn(metadata,table,"createTime",java.util.Date.class, 32, 0, 0, true, false);
			c.setComment("描述");
			table.addColumn(c);
		}
		{
			Column c=buildService.buildColumn(metadata,table,"age","java.lang.Integer", 32, 0, 0, true, false);
			c.setComment("描述");
			table.addColumn(c);
		}
		{
			Column c=buildService.buildColumn(metadata,table,"money","java.math.BigDecimal", 10, 7, 2, true, false);
			c.setComment("描述");
			table.addColumn(c);
		}
		{
			Column c=buildService.buildColumn(metadata,table,"qq","java.lang.Long", 10,0,0, true, false);
			c.setComment("描述");
			table.addColumn(c);
		}
		this.drop();
		{
			String[] sql=buildService.buildCreateTableSql(metadata,table);
			for(String s:sql){
				System.out.println(s);
				jdbcTemplate.execute(s);
			}
		}
	}
	@After
	public void drop(){
		{
			String[] sql=buildService.buldDropTabeSql(metadata,table);
			for(String s:sql){
				System.out.println(s);
				jdbcTemplate.execute(s);
			}
		}
	}
}

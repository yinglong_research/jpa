package com.doll.rs.test.hinernate.mappedid;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.doll.rs.hibernate.HibernateJpaApplication;
import com.doll.rs.hibernate.model.mappedid.Person;
import com.doll.rs.hibernate.model.mappedid.PersonDetails;
import com.doll.rs.hibernate.model.mappedid.PersonDetailsRepo;
import com.doll.rs.hibernate.model.mappedid.PersonRepo;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=HibernateJpaApplication.class)
public class TestInsertPerson {
	
	@Autowired
	private PersonRepo personRepo;
	@Autowired
	private PersonDetailsRepo personDetailsRepo;
	@Test
	public void test(){
		for(int i=0;i<1;i++){
			System.out.print("1111111111111111111111");
			insert(UUID.randomUUID().toString());
		}
	}
//	@Transactional
	public void insert(String i){
		System.out.print("222222222222222");
		Person p=new Person("nick"+i);
		PersonDetails pd=new PersonDetails();
		pd.setPerson(p);
		personDetailsRepo.save(pd);
		pd.setPerson(null);
		System.out.print(pd);
	}
}

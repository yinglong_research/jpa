package com.doll.rs.test.hinernate.mappedby;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.doll.rs.hibernate.HibernateJpaApplication;
import com.doll.rs.hibernate.model.mappedby.A2;
import com.doll.rs.hibernate.model.mappedby.A2Repo;
import com.doll.rs.hibernate.model.mappedby.B2;
import com.doll.rs.hibernate.model.mappedby.B2Repo;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=HibernateJpaApplication.class)
public class TestMappedByA2B2 {
	
	@Autowired
	A2Repo a2Repo;
	@Autowired
	B2Repo b2Repo;
	@Test
	public void test(){
		A2 a2=new A2();
		B2 b2=new B2();
		a2.addB2(b2);
		a2=a2Repo.save(a2);
		
	}
}

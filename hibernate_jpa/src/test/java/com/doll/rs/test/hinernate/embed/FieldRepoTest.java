package com.doll.rs.test.hinernate.embed;

import org.springframework.beans.factory.annotation.Autowired;

import com.doll.rs.hibernate.model.embed.Common;
import com.doll.rs.hibernate.model.embed.Field;
import com.doll.rs.hibernate.model.embed.FieldRepo;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith; 
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.doll.rs.hibernate.HibernateJpaApplication; 
@RunWith(SpringRunner.class)
@SpringBootTest(classes=HibernateJpaApplication.class)
public class FieldRepoTest {
	@Autowired
	FieldRepo fieldRepo;
	
	@Test
	@Transactional
	public void test(){
		Field f=new Field();
		Common c=new Common();
		f.setCommon(c);
		fieldRepo.saveAndFlush(f);
		Field newField=fieldRepo.findOne(f.getId());
		System.out.println(newField.getCommon());
		assert null!=newField.getCommon();
	}
	
	
}

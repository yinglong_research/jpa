package com.doll.rs.test.hinernate.embeddableCollections;


import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.doll.rs.hibernate.HibernateJpaApplication;
import com.doll.rs.hibernate.model.embeddableCollections.Person2;
import com.doll.rs.hibernate.model.embeddableCollections.Person2Repo;
import com.doll.rs.hibernate.model.embeddableCollections.Person2Service;
import com.doll.rs.hibernate.model.embeddableCollections.Phone;
@RunWith(SpringRunner.class)
@SpringBootTest(classes=HibernateJpaApplication.class)
public class TestEmbeddableCollections {
	@Autowired
	Person2Repo person2Repo;
	@Autowired
	Person2Service person2Service;
//	@Test
	public void test(){
		for(int i=0;i<10;i++){
			person2Service.insert();
		}
	}
	@Test
	public void testDelete(){
		
		Person2 p=person2Service.insert();
		List<Phone> phones=p.getPhones();
		phones.remove(1);
		person2Repo.save(p);
	}
	
}

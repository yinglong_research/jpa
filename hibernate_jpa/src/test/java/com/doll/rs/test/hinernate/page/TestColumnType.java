package com.doll.rs.test.hinernate.page;

import java.lang.reflect.Field;
import java.util.Comparator;
import java.util.Map;

import org.hibernate.type.BasicType;
import org.hibernate.type.BasicTypeRegistry;
import org.junit.Test;

public class TestColumnType {
	@Test
	public void test(){
		try {
			BasicTypeRegistry basicTypeRegistry=new BasicTypeRegistry();
			Field f=BasicTypeRegistry.class.getDeclaredField("registry");
			f.setAccessible(true);
			Map<String, BasicType> registry=(Map<String, BasicType>)f.get(basicTypeRegistry);
			registry.entrySet().stream().sorted(new Comparator<Map.Entry<String, BasicType>>(){
				@Override
				public int compare(Map.Entry<String, BasicType> e1, Map.Entry<String, BasicType> e2) {
					return e1.getValue().getName().compareTo(e2.getValue().getName());
				}
			}).forEachOrdered(e->{
//				System.out.println(e.getKey()+"\t"+e.getValue().getName());
			});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

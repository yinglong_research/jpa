package com.doll.rs.test.hinernate.property;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.doll.rs.hibernate.HibernateJpaApplication;
import com.doll.rs.hibernate.model.property.VariableService;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = HibernateJpaApplication.class)
public class TestVariableImpl {
	@Autowired
	VariableService  variableService;
	@Test
	public void test(){
		variableService.addVariable("test", "mykey", 123);
		Integer value=variableService.getVariable("test", "mykey");
		System.out.println(value);
	}
}

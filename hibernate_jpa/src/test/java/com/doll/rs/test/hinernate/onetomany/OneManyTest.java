package com.doll.rs.test.hinernate.onetomany;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import com.doll.rs.hibernate.HibernateJpaApplication;
import com.doll.rs.hibernate.model.onetomany.Many;
import com.doll.rs.hibernate.model.onetomany.ManyRepo;
import com.doll.rs.hibernate.model.onetomany.One;
import com.doll.rs.hibernate.model.onetomany.OneRepo;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = HibernateJpaApplication.class)
public class OneManyTest {
	@Autowired
	ManyRepo manyRepo;
	@Autowired
	OneRepo oneRepo;

	@Test
	public void test() {
		{
			One one = new One();
			for(int i=0;i<1;i++){
				Many m = new Many();
				one.add(m);
			}
			one=oneRepo.save(one);
//			System.out.println(one.getId());
//			one.remove(new Many(1L));
//			one=oneRepo.save(one);
//			one.setName("123");
//			oneRepo.save(one);
			Pageable pageable=new PageRequest(0,2);
			oneRepo.findAll(pageable);
			
		}
	}
}

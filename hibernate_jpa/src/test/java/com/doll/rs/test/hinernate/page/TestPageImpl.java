package com.doll.rs.test.hinernate.page;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManagerFactory;

import org.hibernate.SessionFactory;
import org.hibernate.SessionFactoryObserver;
import org.hibernate.boot.spi.MetadataImplementor;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.internal.SessionFactoryObserverChain;
import org.hibernate.jpa.HibernateEntityManagerFactory;
import org.hibernate.jpa.boot.internal.EntityManagerFactoryBuilderImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.vendor.SpringHibernateJpaPersistenceProvider;
import org.springframework.test.context.junit4.SpringRunner;

import com.doll.rs.hibernate.HibernateJpaApplication;
import com.doll.rs.hibernate.model.page.TestModel;
import com.doll.rs.hibernate.model.page.TestModelRepo;
import org.springframework.jdbc.core.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = HibernateJpaApplication.class)
public class TestPageImpl {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	@Autowired
	TestModelRepo testModelRepo;
	@Autowired
	EntityManagerFactory emf;
	@Test
	public void test(){
		for(int i=0;i<100;i++){
			TestModel m=new TestModel();
			m.setName("name_"+i);
			testModelRepo.save(m);
		}
		String sql="select * from test_model";
		List<Map<String,Object>> list=getPage(sql,0,10);
		list.forEach(map->{
			map.forEach((k,v)->{
				System.out.print(k+":"+v+";");
			});
			System.out.println();
		});
		
		
		System.out.println();
		
	}
	private List<Map<String,Object>> getPage(String sql,final int startRow,final int size){
		return jdbcTemplate.query(sql, new ResultSetExtractor<List<Map<String,Object>>>(){

			@Override
			public List<Map<String,Object>> extractData(ResultSet rs) throws SQLException, DataAccessException {
				ResultSetMetaData md=rs.getMetaData();
				int c=md.getColumnCount();
				String[] arr=new String[c];
				for(int n=0;n<c;n++){
					arr[n]=md.getColumnName(n+1);
//					System.out.println(md.getColumnTypeName(n+1));
				}
				rs.absolute(startRow);
				int i=0;
				List<Map<String,Object>> list=new ArrayList<>();
				while(rs.next()){
					Map<String,Object> map=new LinkedHashMap<String,Object>();
					for(int n=0;n<arr.length;n++){
						map.put(arr[n],rs.getObject(arr[n]));
					}
					list.add(map);
					if(++i>=size){
						break;
					}
				}
				return list;
			}
			
		});
	}
	
}

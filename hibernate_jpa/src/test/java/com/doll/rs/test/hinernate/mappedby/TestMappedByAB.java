package com.doll.rs.test.hinernate.mappedby;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.doll.rs.hibernate.HibernateJpaApplication;
import com.doll.rs.hibernate.model.mappedby.A;
import com.doll.rs.hibernate.model.mappedby.ARepo;
import com.doll.rs.hibernate.model.mappedby.B;
import com.doll.rs.hibernate.model.mappedby.BRepo;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=HibernateJpaApplication.class)
public class TestMappedByAB {
	
	@Autowired
	ARepo aRepo;
	@Autowired
	BRepo bRepo;
	@Test
	public void test(){
		A a=new A();
		{
			B b=new B();
			b.setA(a);
			a.addB(b);
			a=aRepo.save(a);
//			b=bRepo.save(b);
		}
		{
			B b=new B();
			b.setA(a);
			a.addB(b);
			a=aRepo.save(a);
//			b=bRepo.save(b);
			
		}
//		bRepo.delete(new B(1L));
		List<A> aList=aRepo.findAll();
		for(A aa:aList){
			for(B bb:aa.getB()){
				System.out.println(bb.getId());
			}
		}
	}
}
